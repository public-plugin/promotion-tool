using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[InitializeOnLoad]
public static class InitEditor
{
    private static string _SYMBOL_DEFINE_ANALYTICS_PROMOTION_ = "ENABLE_ANALYTICS_PROMOTION";

    [InitializeOnLoadMethod]
    private static void CheckSetting()
    {
        CheckDataSetting();
        RefreshDefineSymbol();
    }

    private static void RefreshDefineSymbol()
    {


        PlayerSettings.GetScriptingDefineSymbols(UnityEditor.Build.NamedBuildTarget.Android, out string[] arrDefineAndroid);
        PlayerSettings.GetScriptingDefineSymbols(UnityEditor.Build.NamedBuildTarget.iOS, out string[] arrDefineIOS);

        var isExist = false;
        if (PromotionSetting.Instance.EnableAnalytics)
        {
            for (int i = 0; i < arrDefineAndroid.Length; i++)
            {
                if (arrDefineAndroid[i].Equals(_SYMBOL_DEFINE_ANALYTICS_PROMOTION_))
                {
                    isExist = true;
                    break;
                }
            }
            if (!isExist)
            {
                string[] newDefinesAndroid = new string[arrDefineAndroid.Length + 1];
                newDefinesAndroid[0] = _SYMBOL_DEFINE_ANALYTICS_PROMOTION_;
                for (int i = 0; i < arrDefineAndroid.Length; i++)
                {
                    newDefinesAndroid[i + 1] = arrDefineAndroid[i];
                }

                PlayerSettings.SetScriptingDefineSymbols(UnityEditor.Build.NamedBuildTarget.Android, newDefinesAndroid);


            }
            isExist = false;
            for (int i = 0; i < arrDefineIOS.Length; i++)
            {
                if (arrDefineIOS[i].Equals(_SYMBOL_DEFINE_ANALYTICS_PROMOTION_))
                {
                    isExist = true;
                    break;
                }
            }
            if (!isExist)
            {
                string[] newDefinesIOS = new string[arrDefineIOS.Length + 1];
                newDefinesIOS[0] = _SYMBOL_DEFINE_ANALYTICS_PROMOTION_;
                for (int i = 0; i < arrDefineIOS.Length; i++)
                {
                    newDefinesIOS[i + 1] = arrDefineIOS[i];
                }

                PlayerSettings.SetScriptingDefineSymbols(UnityEditor.Build.NamedBuildTarget.iOS, newDefinesIOS);

            }
        }
        else
        {
            for (int i = 0; i < arrDefineAndroid.Length; i++)
            {
                if (arrDefineAndroid[i].Equals(_SYMBOL_DEFINE_ANALYTICS_PROMOTION_))
                {
                    isExist = true;
                    break;
                }
            }
            if (isExist)
            {
                string[] newDefinesAndroid = new string[arrDefineAndroid.Length - 1];
                var newIndex = 0;
                for (int i = 0; i < arrDefineAndroid.Length; i++)
                {
                    if (!arrDefineAndroid[i].Equals(_SYMBOL_DEFINE_ANALYTICS_PROMOTION_))
                    {
                        newDefinesAndroid[newIndex] = arrDefineAndroid[i];
                        newIndex++;
                    }
                }

                PlayerSettings.SetScriptingDefineSymbols(UnityEditor.Build.NamedBuildTarget.Android, newDefinesAndroid);

                
            }
            isExist = false;
            for (int i = 0; i < arrDefineIOS.Length; i++)
            {
                if (arrDefineIOS[i].Equals(_SYMBOL_DEFINE_ANALYTICS_PROMOTION_))
                {
                    isExist = true;
                    break;
                }
            }
            if (isExist)
            {
                string[] newDefinesIOS = new string[arrDefineIOS.Length - 1];
                var newIndex = 0;
                for (int i = 0; i < arrDefineIOS.Length; i++)
                {
                    if (!arrDefineIOS[i].Equals(_SYMBOL_DEFINE_ANALYTICS_PROMOTION_))
                    {
                        newDefinesIOS[newIndex] = arrDefineIOS[i];
                        newIndex++;
                    }
                }

                PlayerSettings.SetScriptingDefineSymbols(UnityEditor.Build.NamedBuildTarget.iOS, newDefinesIOS);

            }
        }
    }


    private static void CheckDataSetting()
    {
        var path = "Assets/Resources/Promotion Setting.asset";
        var setting = AssetDatabase.LoadAssetAtPath<PromotionSetting>(path);
        if (setting == null)
        {
            PromotionSetting.CreateDefaultSetting();
        }
    }
}
