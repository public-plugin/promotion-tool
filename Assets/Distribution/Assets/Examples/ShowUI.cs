using SpiritBomb;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ShowUI : MonoBehaviour
{
    private SBPromotion _promotion;
    private void Start()
    {
        _promotion = new SBPromotion();
    }

#if UNITY_EDITOR

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.LogError("Show instertitial");
            _promotion.ShowInstertitial(() =>
            {
                Debug.LogError("Close Instertitial");
            });
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _promotion.ShowRewardedVideo(() =>
            {
                Debug.LogError("Close rewarded");
            });
        }
    }
#endif
}
