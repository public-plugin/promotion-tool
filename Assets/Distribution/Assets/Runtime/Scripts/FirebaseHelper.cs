using Firebase;
using Firebase.RemoteConfig;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SpiritBomb.PromotionTool
{

    public class FirebaseHelper : MonoBehaviour
    {
        private string _apiKey;
        private string _appId;
        private string _projectId;
        private string _storageBucket;

        public static bool IsLoadDone;


        private static FirebaseApp _firebaseApp;

        private Dictionary<string, object> _remoteConfigDefault = new Dictionary<string, object>()
    {
        {"list_app_promotion","" }
    };

        private void Awake()
        {
            return;
#if UNITY_EDITOR || UNITY_ANDROID
            _apiKey = "AIzaSyCqUiwavefjsVQQKQdRuj31HjbPRGaLQrE";
            _appId = "1:519729822660:android:c26ea4c8fb777fecf90291";
            _projectId = "sb-xpromo";
            _storageBucket = "sb-xpromo.appspot.com";
#elif UNITY_IOS
        _apiKey = "AIzaSyB4Zu09FHFO9YdLXBMe7ZFpFFN8sMmdmH8";
        _appId = "1:519729822660:ios:76608cd4d5981512f90291";
        _projectId = "sb-xpromo";
        _storageBucket = "sb-xpromo.appspot.com";
#endif


            Firebase.AppOptions options = new Firebase.AppOptions
            {
                ApiKey = _apiKey,
                AppId = _appId,
                ProjectId = _projectId,
                StorageBucket = _storageBucket,
            };

            _firebaseApp = Firebase.FirebaseApp.Create(options, "firebase-promotion");
            CheckDependencies();

        }

        public static string GetValueRemoteConfig(string key)
        {
            var vls = FirebaseRemoteConfig.GetInstance(_firebaseApp).AllValues;
            var iterator = vls.GetEnumerator();
            while (iterator.MoveNext())
            {
                Debug.LogError(" vl " + iterator.Current.Value.StringValue);
            }


            return FirebaseRemoteConfig.GetInstance(_firebaseApp).GetValue(key).StringValue;
        }


        private void CheckDependencies()
        {
            try
            {
                Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
                {
                    var dependencyStatus = task.Result;
                    if (dependencyStatus == DependencyStatus.Available)
                    {
                        InitFirebase();
                        IsLoadDone = true;
                    }
                    else
                    {
                        Debug.LogError(
                            "Could not resolve all Firebase dependencies: " + dependencyStatus);
                    }
                });
            }
            catch (Exception e)
            {
                Debug.LogError("Check firebase dependencies:" + e.Message);
            }
        }

        private void InitFirebase()
        {
            Firebase.RemoteConfig.FirebaseRemoteConfig.GetInstance(_firebaseApp).SetDefaultsAsync(_remoteConfigDefault).ContinueWith(task =>
            {
                FetchDataRemoteConfig();
            });
        }

        private void FetchDataRemoteConfig()
        {
            TimeSpan fetchCacheExpiration = TimeSpan.FromMinutes(5);
            System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.GetInstance(_firebaseApp).FetchAsync(
                fetchCacheExpiration);
            fetchTask.ContinueWith(FetchComplete);
        }

        private void FetchComplete(System.Threading.Tasks.Task fetchTask)
        {
            var info = Firebase.RemoteConfig.FirebaseRemoteConfig.GetInstance(_firebaseApp).Info;

            Debug.LogError("fetch complete " + info.LastFetchStatus);
            switch (info.LastFetchStatus)
            {
                case Firebase.RemoteConfig.LastFetchStatus.Success:
                    Firebase.RemoteConfig.FirebaseRemoteConfig.GetInstance(_firebaseApp).FetchAndActivateAsync();

                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case Firebase.RemoteConfig.FetchFailureReason.Error:
                            break;
                        case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                            break;
                    }
                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Pending:
                    break;
            }
        }
    }
}
