using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpiritBomb
{

    public class SBPromotion
    {

        private SBPromotionObject _mainObject;

        public SBPromotion()
        {
             _mainObject = Resources.Load<SBPromotionObject>("PromotionAds").CreateInstance();
        }

        public void ShowInstertitial(Action OnCompleted)
        {
            _mainObject = SBPromotionObject.Instance;
            _mainObject.ShowInstertitial(OnCompleted);
        }

        public void ShowRewardedVideo(Action OnCompleted)
        {
            _mainObject = SBPromotionObject.Instance;
            _mainObject.ShowRewardedVideo(OnCompleted);
        }

        public void ShowBanner()
        {
            _mainObject = SBPromotionObject.Instance;
            _mainObject.ShowBanner();
        }

        public void HideBanner()
        {
            _mainObject = SBPromotionObject.Instance;
            _mainObject.HideBanner();
        }
    }
}