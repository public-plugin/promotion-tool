using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PromotionSetting : ScriptableObject
{
    public static PromotionSetting Instance
    {
        get
        {
            if (_instance == null)
            {
#if UNITY_EDITOR
                var path = "Assets/Resources/Promotion Setting.asset";
                _instance = AssetDatabase.LoadAssetAtPath<PromotionSetting>(path);

                if (_instance != null)
                {
                    return Instance;
                }
                CreateDefaultSetting();
#else
                _instance = Resources.Load<PromotionSetting>("Promotion Setting");
#endif
            }
            return _instance;
        }
    }
#if UNITY_EDITOR
    public static void CreateDefaultSetting()
    {
        var path = "Assets/Resources/Promotion Setting.asset";
        _instance = CreateInstance<PromotionSetting>();
        AssetDatabase.CreateAsset(_instance, path);
    }
#endif

    private static PromotionSetting _instance;

    public bool EnableAnalytics = true;
}
