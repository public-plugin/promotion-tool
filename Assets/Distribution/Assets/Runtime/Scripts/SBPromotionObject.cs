using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SBPromotionObject : MonoBehaviour
{
    [SerializeField]
    private float _timeCountDownInstertitial = 5f;

    [SerializeField]
    private float _timeCountDownRewarded = 10f;


    [SerializeField]
    private SBListAppPromotion _listAppPromotion;

    [SerializeField]
    private Transform _rootBanner;
    [SerializeField]
    private Transform _rootInstertitial;
    [SerializeField]
    private Transform _rootRewardVideo;

    [SerializeField]
    private Image _imgBanner;

    [SerializeField]
    private RawImage _imgInstertitial;

    [SerializeField]
    private Text _txtTimeInstertial;

    [SerializeField]
    private Image _imgCountDownInstertitial;

    [SerializeField]
    private Button _btnCloseInstertitial;

    [SerializeField]
    private GameObject _btnInstallInstertitial;

    [SerializeField]
    private GameObject _rootCountDownInstertitial;

    [SerializeField]
    private RawImage _imgReward;

    [SerializeField]
    private Text _txtTimeReward;

    [SerializeField]
    private Image _imgCountDownReward;

    [SerializeField]
    private Button _btnCloseReward;

    [SerializeField]
    private GameObject _btnInstallReward;

    [SerializeField]
    private GameObject _rootCountDownReward;

    [SerializeField]
    private RectTransform _mainRect;

    private string _linkBanner;
    private string _linkInstertitial;
    private string _linkReward;

    private string _appDestBanner;
    private string _appDestInstertitial;
    private string _appDestRewarded;

    private Action _onCompletedInstertitial;
    private Action _onCompletedReward;

    private string _impressionBanner;
    private string _clickBanner;

    private string _impressionInstertitial;
    private string _clickInstertitial;

    private string _impressionRewarded;
    private string _clickRewarded;


    public SBPromotionObject CreateInstance()
    {
        if (_instance == null)
        {
            _instance = Instantiate<SBPromotionObject>(this);
        }
        return _instance;
    }

    public static SBPromotionObject Instance => _instance;
    private static SBPromotionObject _instance;
    private void Awake()
    {
        if (_instance != this && _instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
        InitLayout();
    }


    private void InitLayout()
    {
        _rootBanner.gameObject.SetActive(false);
        _rootInstertitial.gameObject.SetActive(false);
        _rootRewardVideo.gameObject.SetActive(false);
    }


    public void ShowInstertitial(Action OnCompleted)
    {

        EventInsterStart();
        _rootInstertitial.gameObject.SetActive(true);
        _onCompletedInstertitial = OnCompleted;
        _btnCloseInstertitial.gameObject.SetActive(false);
        _rootCountDownInstertitial.gameObject.SetActive(true);
        _btnCloseInstertitial.onClick.AddListener(HideInstertitial);
        StartCoroutine(CoroutineCountDownInstertitial());
        RefreshInstertitial();

    }

    public void ShowRewardedVideo(Action OnCompleted)
    {
        EventRewardStart();
        _rootRewardVideo.gameObject.SetActive(true);
        _onCompletedReward = OnCompleted;
        _btnCloseReward.gameObject.SetActive(false);
        _rootCountDownReward.gameObject.SetActive(true);
        _btnCloseReward.onClick.AddListener(HideReward);
        StartCoroutine(CoroutineCountDownReward());
        RefreshRewardedVideos();
    }

    public void ShowBanner()
    {
        _rootBanner.gameObject.SetActive(true);
        RefreshBanner();
    }

    public void HideBanner()
    {
        _rootBanner.gameObject.SetActive(false);
    }

    private void RefreshBanner()
    {
        var listConfig = _listAppPromotion.ListConf;
        var rnd = UnityEngine.Random.Range(0, listConfig.Count);
        var config = listConfig[rnd];
        while (config.BundleID == Application.identifier)
        {
            rnd = UnityEngine.Random.Range(0, listConfig.Count);
            config = listConfig[rnd];
        }
        var listUriIOS = config.BannerUri;
        var uri = listUriIOS[UnityEngine.Random.Range(0, listUriIOS.Length)];
        var path = uri.Replace(SBConfigPromotion._PREFIX_SPRITE_, "");
        _imgBanner.sprite = Resources.Load<Sprite>(path);
        _linkBanner = config.StoreURL;
        _appDestBanner = config.AppName;

        _clickBanner = config.ClickTrackingLink;
        _impressionBanner = config.ImpressionTrackingLink;
        TrackingImpression(_impressionBanner);
    }

    private void RefreshRewardedVideos()
    {
        var listConfig = _listAppPromotion.ListConf;
        var rnd = UnityEngine.Random.Range(0, listConfig.Count);
        var config = listConfig[rnd];
        while (config.BundleID == Application.identifier)
        {
            rnd = UnityEngine.Random.Range(0, listConfig.Count);
            config = listConfig[rnd];
        }
        var listUriAndroid = config.RewardedUri;
        var uri = listUriAndroid[UnityEngine.Random.Range(0, listUriAndroid.Length)];

        var texture = SBConfigPromotion.GetTexture(uri);
        SetImageReward(texture);
        _linkReward = config.StoreURL;
        _appDestRewarded = config.AppName;
        _impressionRewarded = config.ImpressionTrackingLink;
        _clickRewarded = config.ClickTrackingLink;
        TrackingImpression(_impressionRewarded);
    }

    private void RefreshInstertitial()
    {
        var listConfig = _listAppPromotion.ListConf;
        var rnd = UnityEngine.Random.Range(0, listConfig.Count);
        var config = listConfig[rnd];
        while (config.BundleID == Application.identifier)
        {
            rnd = UnityEngine.Random.Range(0, listConfig.Count);
            config = listConfig[rnd];
        }
        var listUriAndroid = config.InstertitialUri;
        var uri = listUriAndroid[UnityEngine.Random.Range(0, listUriAndroid.Length)];
        var texture = SBConfigPromotion.GetTexture(uri);
        SetImageInstertitial(texture);
        _linkInstertitial = config.StoreURL;
        _appDestInstertitial = config.AppName;

        _impressionInstertitial = config.ImpressionTrackingLink;
        _clickInstertitial = config.ClickTrackingLink;
        TrackingImpression(_impressionInstertitial);
    }

    private void HideInstertitial()
    {
        EventInsterClose();
        if (_onCompletedInstertitial != null)
        {
            _onCompletedInstertitial.Invoke();
        }
        _rootInstertitial.gameObject.SetActive(false);
        _btnCloseInstertitial.onClick.RemoveAllListeners();
    }

    private void HideReward()
    {
        EventRewardClose();
        if (_onCompletedReward != null)
        {
            _onCompletedReward.Invoke();
        }
        _rootRewardVideo.gameObject.SetActive(false);
        _btnCloseReward.onClick.RemoveAllListeners();

    }

    public void ClickBanner()
    {
        EventBannerClick();
        if (!string.IsNullOrEmpty(_linkBanner) && !string.IsNullOrWhiteSpace(_linkBanner))
        {
            if (!string.IsNullOrEmpty(_clickBanner))
            {
                Application.OpenURL(_clickBanner);
            }
            else
            {
                Application.OpenURL(_linkBanner);
            }
        }
    }

    public void ClickInstertial()
    {
        EventInsterClick();
        if (!string.IsNullOrEmpty(_linkInstertitial) && !string.IsNullOrWhiteSpace(_linkInstertitial))
        {
            if (!string.IsNullOrEmpty(_clickInstertitial))
            {
                Application.OpenURL(_clickInstertitial);
            }
            else
            {
                Application.OpenURL(_linkInstertitial);
            }
        }
    }

    public void ClickReward()
    {
        EventRewardClick();
        if (!string.IsNullOrEmpty(_linkReward) && !string.IsNullOrWhiteSpace(_linkReward))
        {
            if (!string.IsNullOrEmpty(_clickRewarded))
            {
                Application.OpenURL(_clickRewarded);
            }
            else
            {
                Application.OpenURL(_linkReward);
            }
        }
    }

    private IEnumerator CoroutineCountDownInstertitial()
    {
        var countDown = _timeCountDownInstertitial;
        var nextValue = Mathf.FloorToInt(countDown - 1);
        _txtTimeInstertial.text = Mathf.CeilToInt(countDown).ToString();
        while (countDown > 0)
        {
            if (countDown <= nextValue)
            {
                nextValue = Mathf.CeilToInt(countDown - 1);
                _txtTimeInstertial.text = Mathf.CeilToInt(countDown).ToString();
            }
            _imgCountDownInstertitial.fillAmount = countDown / _timeCountDownInstertitial;
            yield return new WaitForSecondsRealtime(0.02f);
            countDown -= 0.02f;

        }
        _btnCloseInstertitial.gameObject.SetActive(true);
        _rootCountDownInstertitial.gameObject.SetActive(false);
    }

    private IEnumerator CoroutineCountDownReward()
    {
        var countDown = _timeCountDownRewarded;
        var nextValue = Mathf.FloorToInt(countDown - 1);
        _txtTimeReward.text = Mathf.CeilToInt(countDown).ToString();
        while (countDown > 0)
        {
            if (countDown <= nextValue)
            {
                nextValue = Mathf.CeilToInt(countDown - 1);
                _txtTimeReward.text = Mathf.CeilToInt(countDown).ToString();
            }
            yield return new WaitForSecondsRealtime(0.02f);
            _imgCountDownReward.fillAmount = countDown / _timeCountDownRewarded;
            countDown -= 0.02f;

        }
        _btnCloseReward.gameObject.SetActive(true);
        _rootCountDownReward.gameObject.SetActive(false);

    }

    private IEnumerator CoroutineCountRefreshBanner()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(60);            
            RefreshBanner();
        }
    }

    private void SetImageReward(Texture texture)
    {
        var widthRect = _mainRect.rect.width;
        var heightRect = _mainRect.rect.height;

        var texWidth = texture.width;
        var texHeight = texture.height;

        var ratioWidth = widthRect / texWidth;
        var ratioHeight = heightRect / texHeight;
        _imgReward.texture = texture;

        if (ratioWidth < ratioHeight)
        {
            _imgReward.rectTransform.sizeDelta = new Vector2(texWidth * ratioWidth, texHeight * ratioWidth);
        }
        else
        {
            _imgReward.rectTransform.sizeDelta = new Vector2(texWidth * ratioHeight, texHeight * ratioHeight);
        }
    }

    private void SetImageInstertitial(Texture texture)
    {
        var widthRect = _mainRect.rect.width;
        var heightRect = _mainRect.rect.height;

        var texWidth = texture.width;
        var texHeight = texture.height;

        var ratioWidth = widthRect / texWidth;
        var ratioHeight = heightRect / texHeight;

        _imgInstertitial.texture = texture;

        if (ratioWidth < ratioHeight)
        {
            _imgInstertitial.rectTransform.sizeDelta = new Vector2(texWidth * ratioWidth, texHeight * ratioWidth);
        }
        else
        {
            _imgInstertitial.rectTransform.sizeDelta = new Vector2(texWidth * ratioHeight, texHeight * ratioHeight);
        }
    }


    private void EventInsterStart()
    {
        return;
#if ENABLE_ANALYTICS_PROMOTION
        var dict = new Dictionary<string, string>();
        var pr = new Firebase.Analytics.Parameter("app_dest", _appDestInstertitial);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("xpromo_fs_ad_start", pr);
#endif
    }

    private void EventInsterClose()
    {
        return;
#if ENABLE_ANALYTICS_PROMOTION
        var dict = new Dictionary<string, string>();
        var pr = new Firebase.Analytics.Parameter("app_dest", _appDestInstertitial);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("xpromo_fs_ad_close", pr);
#endif
    }

    private void EventInsterClick()
    {
        return;
#if ENABLE_ANALYTICS_PROMOTION
        var dict = new Dictionary<string, string>();
        var pr = new Firebase.Analytics.Parameter("app_dest", _appDestInstertitial);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("xpromo_fs_ad_click", pr);
#endif
    }

    private void EventRewardStart()
    {
        return;
#if ENABLE_ANALYTICS_PROMOTION
        var dict = new Dictionary<string, string>();
        var pr = new Firebase.Analytics.Parameter("app_dest", _appDestRewarded);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("xpromo_rv_ad_start", pr);
#endif
    }

    private void EventRewardClose()
    {
        return;
#if ENABLE_ANALYTICS_PROMOTION
        var dict = new Dictionary<string, string>();
        var pr = new Firebase.Analytics.Parameter("app_dest", _appDestRewarded);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("xpromo_rv_ad_close", pr);
#endif
    }
    private void EventRewardClick()
    {
        return;
#if ENABLE_ANALYTICS_PROMOTION
        var dict = new Dictionary<string, string>();
        var pr = new Firebase.Analytics.Parameter("app_dest", _appDestRewarded);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("xpromo_rv_ad_click", pr);
#endif
    }

    private void EventBannerClick()
    {
        return;
#if ENABLE_ANALYTICS_PROMOTION
        var dict = new Dictionary<string, string>();
        var pr = new Firebase.Analytics.Parameter("app_dest", _appDestBanner);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("xpromo_bn_ad_click", pr);
#endif
    }

    private void TrackingImpression(string link)
    {
        StartCoroutine(CoroutineGetRequest(link));
    }

    private IEnumerator CoroutineGetRequest(string uri)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(uri))
        {
            yield return request.SendWebRequest();
        }
    }
}
