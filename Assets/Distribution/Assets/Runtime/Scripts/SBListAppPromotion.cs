using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SBConfigPromotion
{
    public string AppName;
    public string AndroidUrl;
    public string AndroidBundle;
    public int AndroidWeight;

    public string IOSUrl;
    public int IOSWeight;
    public string IOSBundle;

    public string[] BannerUriAndroid;
    public string[] BannerUriIOS;

    public string[] InstertitialUriAndroid;
    public string[] InstertitialUriIOS;

    public string[] RewardedUriAndroid;
    public string[] RewardedUriIOS;


    public static string _PREFIX_SPRITE_ = "spr://";
    public static string _PREFIX_IMG_LINK_ = "img_url://";
    public static string _PREFIX_SPRITE_BYTES_ = "bytes://";
    public static string _PREFIX_VIDEO_LINK_ = "vdo://";

    public static Texture2D GetTexture(string path)
    {
        if (path.Contains(_PREFIX_SPRITE_))
        {
            return GetTextureFromSprite(path);
        }
        else
            return GetTextureFromBytes(path);
    }

    public static Texture2D GetTextureFromBytes(string path)
    {
        Texture2D rt = new Texture2D(2, 2, TextureFormat.RGB24, false);

        if (!path.Contains(_PREFIX_SPRITE_BYTES_))
        {
            throw new System.Exception("Sai link resource");
        }
        var realPath = path.Replace(_PREFIX_SPRITE_BYTES_, "");
        var tex = Resources.Load<TextAsset>(realPath);
        rt.LoadImage(tex.bytes);
        Resources.UnloadAsset(tex);
        return rt;
    }

    public static Texture2D GetTextureFromSprite(string path)
    {
        if (!path.Contains(_PREFIX_SPRITE_))
        {
            throw new System.Exception("Sai link resource sprite");
        }
        var realPath = path.Replace(_PREFIX_SPRITE_, "");
        var sprite = Resources.Load<Sprite>(realPath);
        return sprite.texture;
    }
}

[System.Serializable]
[CreateAssetMenu(fileName = "List App Promotion", menuName = "Spirit Bomb/Promotion/Data", order = 1)]
public class SBListAppPromotion : ScriptableObject
{
    [SerializeField]
    private List<SBConfigApp> _listConfigAndroid;
    [SerializeField]
    private List<SBConfigApp> _listConfigIOS;

    public List<SBConfigApp> ListConf
    {
        get
        {
#if UNITY_EDITOR || UNITY_ANDROID
            return _listConfigAndroid;
#elif UNITY_IOS
            return _listConfigIOS;
#else
            return null;
#endif
        }
    }
}

[System.Serializable]
public class SBConfigApp
{
    public string AppName;
    public string BundleID;
    public string StoreURL;
    public int WeightRandom;

    public string[] BannerUri;
    public string[] InstertitialUri;
    public string[] RewardedUri;

    public string ImpressionTrackingLink;
    public string ClickTrackingLink;
}